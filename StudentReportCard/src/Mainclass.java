

import java.util.Scanner;

class Student{
	int studentId;
	String studentName;
	String departmentName;
	int semesterNo;
	public Student(int studentId, String studentName, String departmentName, int semesterNo) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.departmentName = departmentName;
		this.semesterNo = semesterNo;
	}
	public void display(){
		System.out.println("----------------------------------------------");
		System.out.println("Student ID:"+this.studentId);
		System.out.println("----------------------------------------------");
		System.out.println("studentName:"+this.studentName);
		System.out.println("----------------------------------------------");
		System.out.println("departmentName:"+this.departmentName);
		System.out.println("----------------------------------------------");
		System.out.println("semesterNo:"+this.semesterNo);
		System.out.println("----------------------------------------------");
	}
}
class StudentReportCard extends Student{
	int total_subjects;
	int points_earned;
	float gpa;
	float cgpa;
	public StudentReportCard(int studentId, String studentName, String departmentName, int semesterNo,int total_subjects,int points_earned) {
		super(studentId, studentName, departmentName, semesterNo);
		this.total_subjects = total_subjects;
		this.points_earned = points_earned;
	}
float gpaCalculator(){
	float GPA;
	GPA=(float)((this.total_subjects*this.points_earned)/7);
	return GPA;
}
float cgpaCalculator(){
	float CGPA;
	float GPA=gpaCalculator();
	CGPA=(float)GPA/this.semesterNo;
	return CGPA;
}
public void display(){
	System.out.println("----------------------------------------------");
	System.out.println("Student ID:"+this.studentId);
	System.out.println("----------------------------------------------");
	System.out.println("NameOfTheStudent:"+this.studentName);
	System.out.println("----------------------------------------------");
	System.out.println("departmentName:"+this.departmentName);
	System.out.println("----------------------------------------------");
	System.out.println("semesterNo:"+this.semesterNo);
	System.out.println("----------------------------------------------");
	System.out.println("GPA Computed:"+gpaCalculator());
	System.out.println("----------------------------------------------");
	System.out.println("CGPA Computed:"+cgpaCalculator());
	System.out.println("----------------------------------------------");
}
}
public class Mainclass{
	boolean validateInputs(int studentID, int semNumber, int total_subjects, int points_earned){
		String sid=String.valueOf(studentID);
		String snumber=String.valueOf(semNumber);
		String tsubjects=String.valueOf(total_subjects);
		String pointEarned=String.valueOf(points_earned);
		if((sid.length()<=4) && (snumber.length()>=1) && (snumber.length()<=7)&&(tsubjects.length()>=1) && (tsubjects.length()<=7)&&(pointEarned.length()>=1) && (pointEarned.length()<=20) ){
			return true;
		}
		else{
			return false;
		}
	}
public static void main(String[] args) {
	Scanner in=new Scanner(System.in);
	int studentID=in.nextInt();
	int semNumber=in.nextInt();
	int total_subjects=in.nextInt();
	int points_earned=in.nextInt();
	Mainclass main=new Mainclass();
	if(main.validateInputs(studentID, semNumber, total_subjects, points_earned)==true){
	StudentReportCard report=new StudentReportCard(studentID,"Hariprasath P","CSE",semNumber,total_subjects, points_earned);
	report.display();
	}
	else{
		System.out.println("Invalid Request");
	}
}
}
